# Grey matter density extration
- The present scripts can get GM densities (mean and SD) from multiple ROIs
- The images should be in .nii.gz format. Registered to MNI space.

## Requirements
numpy
pandas
nibabel
os


## Brainnetome_v1.0.2 - ROI definitions
Brainnetome atlas, original paper: https://academic.oup.com/cercor/article/26/8/3508/2429104)



