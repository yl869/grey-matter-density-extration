import os
from os.path import join as join
import numpy as np
import pandas as pd
import nibabel.processing
import nibabel as nib


def mask_gm_density(full_nii_path, atlas_path, probability_map_threshold=25, save_binary_masks=False, save_masked_niis=False):
    """
    Function to parcellate whole-brain grey matter density into subregions as defined by the brainnetome atlas (tested with v1.0.2). Note this
    function does not return anything but writes the results (and masks if requested) to the specified out_path.

    By default, the atlas (probability map) is resliced to the same space as the nifti using nibabel's resample_from_to() with default settings. This will
    resalmpe the probability map with a 3rd order spline.

    Next the probability map is binarized with a default threshold of 25% based on the brainnetome paper (https://academic.oup.com/cercor/article/26/8/3508/2429104).

    Finally the GM density in the region of the mask is extracted and the average and SD are saved.

    Options for saving a nifti of the masked input nii are provided. Note that depending on the size of the input nii this can be large eg. 5-6 GB as there are 246 masks.

    INPUTS:
        full_nii_path - full path to the nii to mask
        atlas_path - path to the base directory of the brainnetome atlas (e.g. ../Brainnetome_v1.0.2/)
        probability_map_threshold - threshold for binarizing the probability maps. Default take from brainetome paper
        save_binary_masks - if a path to a directory is passed, a nii of every thresholded mask will be saved to the output dir
        save_masked_niis - if a path to a directory is passed, a nii of every extracted region will be saved to the output dir

    OUTPUTS:
        densities_as_two_col_table - a table of masked densities with row names are masked region names,
                                     column 1 is mean value of the passed nii in the region and column 2 is the standard deviations

        if save_binary_masks, save_masked_niis inputs are used, niis will be written to the output dir.
        if save_binary_masks, save_masked_niis inputs are used, 'binarized_mask' and 'masked_nii_data' need to be pass from get_roi_densities_within_roi (line93) to line 58

    NOTES:
        The resample setp is crucial even if niis are nominally in the same space (e.g. with SPM resize_img),
        sometimes voxel dim is flipped pos / negative. This doesn't seem to matter when working with
        the raw data matricies with matlabs NIFTItools but if the affine matricies are
        not exactly the same the results are completely out in nibabel.

        Run with 'python extract_densities_within_rois.py'

    """
    check_paths([full_nii_path, atlas_path, save_binary_masks, save_masked_niis])

    list_of_regions = np.loadtxt(join(atlas_path, "Atlas", "atlas.txt"), dtype="str")

    nii = nib.load(full_nii_path)

    extracted_roi_densities = {}
    for mask_num, mask_name in list_of_regions:

        mask = nib.load(join(atlas_path, "Atlas", "PM",
                             mask_num + ".nii.gz"))

        density_in_roi, __, binarized_mask, masked_nii_data, mask, nii = get_roi_densities_within_roi(mask, nii, probability_map_threshold)

        extracted_roi_densities[mask_name] = {"mean": np.mean(density_in_roi),
                                              "sd": np.std(density_in_roi, ddof=1)}

        if save_binary_masks:
            binarized_mask_nii = nib.Nifti1Image(binarized_mask, mask.affine, mask.header)
            nib.save(binarized_mask_nii,
                     join(save_binary_masks, "binarized_{0}.nii".format(mask_name)))

        if save_masked_niis:
            masked_nii = nib.Nifti1Image(masked_nii_data, nii.affine, nii.header)
            nib.save(masked_nii,
                     join(save_masked_niis, "nii_masked_{0}.nii".format(mask_name)))

    densities_as_two_col_table = pd.DataFrame(extracted_roi_densities).T
    
   
    return densities_as_two_col_table


def get_roi_densities_within_roi(mask, nii, probability_map_threshold):
    """
    see mask_gm_density()
    """
    mask = nib.processing.resample_from_to(mask, nii)

    mask_indexes = mask.get_fdata(dtype=np.float32) >= probability_map_threshold

    binarized_mask = np.where(mask_indexes, 1, 0)

    masked_nii_data = binarized_mask * nii.get_fdata(dtype=np.float32)

    density_in_roi = masked_nii_data[np.where(masked_nii_data > 0)]

    return density_in_roi, mask_indexes, binarized_mask, masked_nii_data, mask, nii


def check_paths(paths_to_check):

    for path_ in paths_to_check:
        if path_:
            assert os.path.isdir(path_) or os.path.isfile(path_), \
                "{0} is not a directory".format(path_)              




"""
 For a list of subjects, get an csv file for each of them
 participant.txt should include the IDs of all the partisipants to processing
 The images should be in .nii.gz format
"""
list_of_particpants = np.loadtxt(join(os.getcwd(), "participant.txt"), dtype="str")
for sub in list_of_particpants:
    DataFrame = mask_gm_density(os.path.join(os.getcwd(), "data", sub+".nii.gz"), os.path.join(os.getcwd(), "Brainnetome_v1.0.2"), probability_map_threshold=25, save_binary_masks=False, save_masked_niis=False)

    DataFrame.to_csv(os.path.join(os.getcwd(), sub+".csv"))



"""
for one image

DataFrame = mask_gm_density(os.path.join(os.getcwd(), "data", "test.nii.gz"), os.path.join(os.getcwd(), "Brainnetome_v1.0.2"), probability_map_threshold=25, save_binary_masks=False, save_masked_niis=False)

DataFrame.to_csv(os.path.join(os.getcwd(), "out.csv"))
"""
#a = os.path.join(os.getcwd(), "binary_masks_")
#b = os.path.join(os.getcwd(), "masked_nii_")

#DataFrame = mask_gm_density( "/zfs/Cohort_Raw_Data/ADNI/Neuroimaging/proccessing_dir/4085/06-Jul-2011/swc1rADNI_035_S_4085_MR_MT1__GradWarp__N3m_Br_20120405183440178_S113976_I296312.nii", os.path.join(os.getcwd(), "Brainnetome_v1.0.2"), probability_map_threshold=25, save_binary_masks=a, save_masked_niis=b)

#DataFrame.to_csv(os.path.join(os.getcwd(), "out.csv"))
